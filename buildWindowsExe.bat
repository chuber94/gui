
call C:\ProgramData\Miniconda3\Scripts\activate.bat pytorch

cd C:\Users\Christian Huber\.conda\envs\pytorch\Lib\site-packages

ren PythonRecordingClient-2.4.1-py3.8.egg PythonRecordingClient-2.4.1-py3.8.egg2
ren PythonRecordingClient-2.4.1-py3.8.egg3 PythonRecordingClient-2.4.1-py3.8.egg

cd C:\Users\Christian Huber\Documents\ISL\LT\gui

pyinstaller main.py -p "C:\Users\Christian Huber\Documents\ISL\LT\pythonrecordingclient" --add-data "C:\Users\Christian Huber\.conda\envs\pytorch\Lib\site-packages\certifi\cacert.pem;certifi" --name lt_gui_windows_v5 --icon=icon.ico --onefile --windowed

cd C:\Users\Christian Huber\.conda\envs\pytorch\Lib\site-packages

ren PythonRecordingClient-2.4.1-py3.8.egg PythonRecordingClient-2.4.1-py3.8.egg3
ren PythonRecordingClient-2.4.1-py3.8.egg2 PythonRecordingClient-2.4.1-py3.8.egg