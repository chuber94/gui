
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog
from pathlib import Path

from gui import Ui_MainWindow
from main_record import MainWindow as MainWindow_record

from pythonrecordingclient.inputStreamAdapter import PortaudioStream, BaseAdapter
from pythonrecordingclient.mediatorLib import get_worker_list
import json
from pythonrecordingclient.mediatorLib import Auth
import socket
from pythonrecordingclient.session import Session
import types

import time, datetime
from uuid import getnode as get_mac
import threading
import numpy as np
import requests
import os

import python_recording_client as prc

def update_text_edit(text_edit, text, old=True):
    old_text = text_edit.toPlainText()
    text_edit.setText((old_text if old else "")+("\n" if old_text!="" and old else "")+text)
    scrollbar = text_edit.verticalScrollBar()
    scrollbar.setValue(scrollbar.maximum())

def extract_confidence(s):
    s2 = s.split()
    try:
        s2,s3 = s2[::2], [float(x) for x in s2[1::2]]
        if len(s2)!=len(s3):
            return s
        out = " ".join([x if x2>=0.28 else "<U>"+x+"</U>" for x,x2 in zip(s2,s3)])
        #print(s,s2,out)
        return out
    except ValueError:
        return s

def log_to_str(log):
    output = ""
    for i in range(len(log)-1):
        if log[i+1][0]>=log[i][1]:
            output += extract_confidence(log[i][2])+" "
    output += '<span style="color:grey;">'+extract_confidence(log[-1][2])+'</span>'
    output = output.replace("<br><br>","\n").replace("  "," ").replace("\n ","\n")
    return output

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, *args, **kwargs):
        QtWidgets.QMainWindow.__init__(self, *args, **kwargs)
        self.setupUi(self)

        self.pushButton_3.clicked.connect(self.startRecording)
        self.pushButton_4.clicked.connect(self.stopRecording)
        self.pushButton_5.clicked.connect(self.saveText)

        self.checkBox.clicked.connect(self.checkbox1Clicked)
        self.checkBox_3.clicked.connect(self.checkbox2Clicked)
        self.checkBox_7.clicked.connect(self.checkbox3Clicked)

        self.lineEdit_4.currentTextChanged.connect(self.server_editing_finished)
        self.lineEdit_6.editingFinished.connect(self.username_editing_finished)
        self.lineEdit_3.editingFinished.connect(self.password_editing_finished)

        self.comboBox.currentTextChanged.connect(self.input_language_selected)

        self.groupBox_6.hide()
        self.textEdit_3.textChanged.connect(self.new_words_changed)
        self.pushButton.clicked.connect(self.send_new_words)

        self.chunksize = 1024
        self.default_server = self.get_server()
        self.default_port = self.get_port()
        self.server = self.get_server()
        self.port = self.get_port()
        self.username = self.get_username()
        self.password = self.get_password()
        self.authserver = "https://authentication.ira.uka.de"
        self.server_new_words = "https://i13hpc51.ira.uka.de:8080/new_words"

        def language_mapping(values):
            values = [self.languages[0][v].split("_")[0] for v in values]
            tmp = {"de-DE":"German","en-EU":"English","es-ES":"Spanish","fr-FR":"French","it-IT":"Italian","ru-RU":"Russian"}
            return [tmp[v] if v in tmp else v for v in values]
        self.language_mapping = language_mapping

        self.update_audio_sources()
        self.update_languages()

        self.components = [self.comboBox,self.comboBox_2,self.checkBox,self.lineEdit,self.lineEdit_2,self.checkBox_2,self.checkBox_3,self.plainTextEdit_2,self.comboBox_3,self.lineEdit_4,self.lineEdit_6,self.lineEdit_3,self.checkBox_4,self.checkBox_6,self.checkBox_7,self.comboBox_4]

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.showTime)

        self.seconds = 0

        self.lock = threading.Lock()
        self.newLogs = []

        self.old_log = ""
        self.current_log = []

        self.pushButton_5.setEnabled(True)

        try:
            location = QtCore.QStandardPaths.standardLocations(QtCore.QStandardPaths.DocumentsLocation)[0]
            os.mkdir(location+"/LT_audios")
        except FileExistsError:
            pass

        self.actionRecording_only.triggered.connect(self.open_recording_only)

    def open_recording_only(self):
        self.hide()
        self.w = MainWindow_record()
        self.w.show()

    def audio_callback(self, audiofileid: str, chunk: bytes):
        if self.checkBox_5.isChecked():
            with open(audiofileid+'.raw', 'ab') as f:
                f.write(chunk)

    def showTime(self):
        self.seconds += 0.1

        time = QtCore.QTime(0,0,0).addSecs(int(self.seconds))
        self.timeEdit.setTime(time)

        with self.lock:
            log_to_process = self.newLogs
            self.newLogs = []

        if len(log_to_process)>0:
            for s in log_to_process:
                s = s.split("|")
                b,e,t = int(s[3][1:-1]),int(s[4][1:-1]),":".join(s[6].split(":")[3:]).strip()
                if b==0 and e==0:
                    t = [x.replace(" ","").replace("\t"," ") for x in t.split("\n")]
                    self.log("Success: New words "+str(t)+" are used now.")
                    self.textEdit_3.setText("\n".join(t))
                else:
                    self.current_log.append([b,e,t])
            if len(self.current_log)>0:
                current_log = log_to_str(self.current_log)
            
                complete_log = self.old_log+("\n" if self.old_log!="" else "")+current_log
                complete_log = "<html><body>"+complete_log+"</html></body>"
                update_text_edit(self.textEdit, complete_log, old=False)

    def log(self, s):
        update_text_edit(self.textEdit_2, s)

    def input_language_selected(self):
        print("Input language selected")

        index = self.comboBox.currentIndex()
        liste,graph,values,_ = self.languages

        values2 = [j for j,s in enumerate(liste) if s.split("_")[1]=="text" and graph[values[index],j]>0]
        #print(values2)
        self.languages[-1] = values2

        self.comboBox_3.clear()
        self.comboBox_3.addItems(self.language_mapping(values2))

        visible = "memory" in self.get_fingerprint()
        self.groupBox_6.setVisible(visible)

    def get_server(self):
        if self.lineEdit_4.currentIndex()==1:
            self.authserver = "https://mediator1.lectrans.scc.kit.edu"
        else:
            self.authserver = "https://authentication.ira.uka.de"
        return self.lineEdit_4.currentText().split(":")[0]

    def get_port(self):
        try:
            return int(self.lineEdit_4.currentText().split(":")[1])
        except ValueError:
            return self.default_port

    def get_username(self):
        return self.lineEdit_6.text()

    def get_password(self):
        return self.lineEdit_3.text()

    def get_fingerprint(self):
        return self.languages[0][self.languages[2][self.comboBox.currentIndex()]].split("_")[0]

    def server_editing_finished(self):
        tmp = self.get_server()
        if tmp!=self.server:
            self.server = tmp
            if self.server != "":
                self.update_languages()

    def port_editing_finished(self):
        try:
            tmp = self.get_port()
        except ValueError as e:
            tmp = int(self.default_port)

        if tmp!=self.port:
            self.port = tmp
            self.update_languages()

    def username_editing_finished(self):
        tmp = self.get_username()
        if tmp!=self.username:
            self.username = tmp
            self.update_languages()

    def password_editing_finished(self):
        tmp = self.get_password()
        if tmp!=self.password:
            self.password = tmp
            self.update_languages()

    def new_words_changed(self):
        self.pushButton.setEnabled(True)

    def send_new_words(self):
        words = self.textEdit_3.toPlainText()
        self.log("Sending "+str(words.split("\n") if words!="" else [])+" as new words.")

        fingerprint = self.get_fingerprint()

        xmlMsg = '<data type="audio" fingerprint="'+fingerprint+'" subType="words"><words>{}</words></data>'
        prc.ObjectStore.session.interface._send_data_xml(xmlMsg,words,words)

        self.pushButton.setEnabled(False)

    def mediator_error(self):
        self.pushButton_3.setEnabled(False)
        self.comboBox.clear()
        self.comboBox_3.clear()

    def update_languages(self):
        print("Updated languages")
        self.comboBox.clear()
        self.comboBox_3.clear()
        self.pushButton_3.setEnabled(False)
        self.groupBox_6.setVisible(False)

        try:
            port = self.get_port()
        except ValueError as e:
            port = int(self.default_port)

        server = self.get_server()
        if server=="":
            server = self.default_server

        user = self.get_username()
        passw = self.get_password()
        self.auth = Auth(user, passw, self.authserver) if user!="" and passw!="" else None

        try:
            while True:
                try:
                    workers = get_worker_list(server, port, self.auth)
                    if workers is None:
                        if user == "" or passw == "":
                            self.log("Warning: You have to enter username and password.")
                        else:
                            self.log("ERROR: Username and password not correct.")
                        return
                    break
                except AssertionError:
                    print("Warning: Loading workers again")
                time.sleep(0.1)
        except (socket.gaierror,ConnectionRefusedError) as e:
            if "Es konnte keine Verbindung hergestellt werden, da der Zielcomputer die Verbindung verweigerte" in str(e):
                if user == "" or passw == "":
                    self.log("Warning: You have to enter username and password.")
                else:
                    self.log("ERROR: Username and password not correct.")
                return
            self.log("ERROR: Mediator not available, check your internet connection.")
            self.mediator_error()
            return

        if workers is not None:
            s = set()
            for dic in json.loads(workers)['availablequeues']:
                if len(dic)==4:
                    i = dic['inputfingerprint']+"_"+dic['inputtype']
                    i2 = dic['outputfingerprint']+"_"+dic['outputtype']
                    s.add(i)
                    s.add(i2)
            liste = sorted(list(s))
            #print(liste)

            graph = np.eye(len(liste))
            for dic in json.loads(workers)['availablequeues']:
                if len(dic)==4:
                    i = dic['inputfingerprint']+"_"+dic['inputtype']
                    i2 = dic['outputfingerprint']+"_"+dic['outputtype']
                    graph[liste.index(i),liste.index(i2)] += 1
            graph_orig = graph

            nonz = np.count_nonzero(graph)
            while True:
                graph = np.dot(graph_orig,graph)
                nonz2 = np.count_nonzero(graph)
                if nonz2==nonz:
                    break
                nonz = nonz2

            #print(graph)

            values = [i for i,s in enumerate(liste) if s.split("_")[1]=="audio" and any([graph[i,j]>0 for j,s2 in enumerate(liste) if s2.split("_")[1]=="text"])]

            if len(values)==0:
                self.log("ERROR: Not enough workers available at this mediator")
                self.mediator_error()
                return

            self.log("Success: Workers for mediator available")

            self.languages = [liste,graph,values,None]
            #print(self.languages)

            self.comboBox.addItems(self.language_mapping(values)) # invokes input_language_selected

            self.pushButton_3.setEnabled(True)
        else:
            self.log("ERROR: Mediator not available, check your internet connection.")
            self.mediator_error()

    def update_audio_sources(self):
        print("Updated audio")

        stream_adapter = PortaudioStream(chunksize=self.chunksize)
        #ObjectStore.stream_adapter = stream_adapter

        if not stream_adapter.available():
            self.log("ERROR: No audio devices found")
            return

        self.comboBox_2.clear()
        self.audio_devices = [(k,v) for k,v in stream_adapter.get_audio_devices().items()]

        if len(self.audio_devices)==0:
            self.log("ERROR: No audio devices found")
            return
        
        self.log("Success: Audio devices found")

        self.comboBox_2.addItems([v for k,v in self.audio_devices])

        if len(self.audio_devices)>1:
            self.comboBox_2.setCurrentIndex(0)

    def startRecording(self):
        self.pushButton_3.setEnabled(False)
        self.pushButton_4.setEnabled(True)
        self.pushButton_5.setEnabled(False)

        self.states_before_recording = [c.isEnabled() for c in self.components]
        for c in self.components:
            c.setEnabled(False)
        self.textEdit_3.setEnabled(True)

        self.textEdit.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        self.old_log = self.textEdit.toPlainText()
        self.current_log = []

        if not self.checkBox_7.isChecked():
            title = self.lineEdit.text()+("?N=L" if self.checkBox.isChecked() else "")
            if self.checkBox_2.isChecked():
                title += "?C="+str(datetime.date.today())+","+str(get_mac()) if self.checkBox_2.isChecked() else ""
        else:
            event = self.events[self.comboBox_4.currentIndex()]
            title = event[0]+"?N=L?S="+event[1]+"?D="+event[2]+"?C="+event[3] # e.g. "SESSIONNAME?N=L?S=SLUG?D=YYYY-MM-DD HH:MM?C=HALL_SLUG_YYMMDDHHMM"

        prc.ObjectStore.isRunning = 1

        from argparse import Namespace
        args = Namespace()
        args.log = 'info'
        args.user = self.get_username()
        args.passw = self.get_password()
        args.list_workers = False
        args.input = 'portaudio'
        args.audiodevice = self.audio_devices[self.comboBox_2.currentIndex()][0]
        args.list = False
        args.hall = False
        args.outputfile = False
        args.printoutput = True
        args.desc = ""
        args.outfingerprint = self.languages[0][self.languages[3][self.comboBox_3.currentIndex()]].split("-")[0]
        args.fingerprint = self.get_fingerprint()
        args.store = self.checkBox_6.isChecked()
        args.zoomurl = self.plainTextEdit_2.toPlainText()
        if args.zoomurl=="":
            args.zoomurl = None
        args.zoomquickcc = self.checkBox_4.isChecked()
        args.audiochannel = None
        args.server = self.server
        args.port = self.port
        args.proxy = None
        args.chunksize = 1024
        args.password = self.lineEdit_2.text()
        args.gui = self
        args.authserver = self.authserver
        args.callback = self.audio_callback
        args.audiofileid = 'audios/'+str(datetime.datetime.now()).replace(":","_")
        #print(args)

        tmp = ""
        if args.fingerprint[:2]!="en":
            tmp += "en"
        else:
            tmp += "de"
        title += "?T="+tmp
        title += "?SUB="+tmp
        args.title = title

        prc.main(args)

        self.log("Recording started ...")

        self.timer.start(100)

    def stopRecording(self):
        self.log("Recording stoped.")

        self.pushButton_4.setEnabled(False)
        self.pushButton_3.setEnabled(True)
        self.pushButton_5.setEnabled(True)
        self.textEdit.setTextInteractionFlags(QtCore.Qt.TextEditable|QtCore.Qt.TextSelectableByMouse)
        self.textEdit_3.setEnabled(False)

        for i,c in enumerate(self.components):
            c.setEnabled(self.states_before_recording[i])

        prc.ObjectStore.isRunning = 0
        prc.ObjectStore.stream_adapter.convert()

        self.timer.stop()

    def checkbox1Clicked(self):
        #print("Checkbox one clicked "+str(self.checkBox.isChecked()))

        if self.checkBox.isChecked():
            for x in [self.checkBox_7]:
                x.setEnabled(True)
            for x in [self.lineEdit,self.checkBox_3,self.lineEdit_2,self.checkBox_2,self.checkBox_6]:
                x.setEnabled(not self.checkBox_7.isChecked())
            self.lineEdit_2.setEnabled(self.checkBox_3.isChecked() and not self.checkBox_7.isChecked())
            self.comboBox_4.setEnabled(self.checkBox_7.isChecked())
        else:
            for x in [self.checkBox_2,self.checkBox_3,self.lineEdit,self.lineEdit_2,self.checkBox_6,self.checkBox_7,self.comboBox_4]:
                x.setEnabled(False)

    def checkbox2Clicked(self):
        #print("Checkbox two clicked "+str(self.checkBox_3.isChecked()))

        if self.checkBox_3.isChecked():
            for x in [self.lineEdit_2]:
                x.setEnabled(True)
        else:
            for x in [self.lineEdit_2]:
                x.setEnabled(False)
            self.lineEdit_2.setText("")

    def checkbox3Clicked(self):
        #print("Checkbox three clicked "+str(self.checkBox_3.isChecked()))

        self.comboBox_4.clear()

        if self.checkBox_7.isChecked():
            events = requests.get("https://lecture-translator.kit.edu/events/?offset_pre=720&offset_post=720").json()
            self.events = [(e['lecture_term']['lecture']['title'],e['lecture_term']['lecture']['slug'],start.strftime('%Y-%m-%d %H:%M'),u'{}_{}_{}'.format(e['lecture_hall']['name'], e['lecture_term']['lecture']['slug'],start.strftime('%y%m%d%H%M'))) for e in events
                           if (start:=datetime.datetime.strptime(e['start'], '%Y-%m-%dT%H:%M:%S')) and e['lecture_hall']['name']=="Zoom"]

            self.comboBox_4.addItems([e[2]+": "+e[0] for e in self.events])

            for x in [self.comboBox_4]:
                x.setEnabled(True)
            for x in [self.lineEdit,self.checkBox_3,self.lineEdit_2,self.checkBox_2,self.checkBox_6]:
                x.setEnabled(False)
        else:
            for x in [self.comboBox_4]:
                x.setEnabled(False)
            for x in [self.lineEdit,self.checkBox_3,self.checkBox_2,self.checkBox_6]:
                x.setEnabled(True)
            self.lineEdit_2.setEnabled(self.checkBox_3.isChecked())

    def saveText(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file', str(Path.home()))
        try:
            with open(fname[0],"w") as f:
                f.write(self.textEdit.toPlainText())
            self.log("Success: Text saved to "+fname[0])
        except FileNotFoundError:
            self.log("ERROR: The chosen file does not exist.")

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
