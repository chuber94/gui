#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Dependency: PyAudio (make sure to have macports 2014 version installed, 2011 version won't work)
# Example:
# Usage:
# - First list all available audio devices in order to select the correct one:
# > python recording_client.py -L 1
# Start recording using default parameters and audio device 1:
# > python recording_client.py -S i13srv30.ira.uka.de -d Description -a 1
# Or select audio device via specifying the (partial) name:
# > python recording_client.py -S i13srv30.ira.uka.de -d Description -a microph

import argparse
import logging
import os
import signal
import sys
import urllib.request, urllib.parse, urllib.error
import numpy as np
from datetime import datetime
from threading import Thread, Lock
from typing import Union, List, Dict, Optional, BinaryIO
import scipy.io.wavfile as wf

import pyaudio # type: ignore
import requests

from pythonrecordingclient.session import Session, on_session_ready, on_receive
from pythonrecordingclient.mediatorLib import Auth
from pythonrecordingclient.config import Config
from pythonrecordingclient.inputStreamAdapter import BaseAdapter, PortaudioStream, FfmpegStream
from pythonrecordingclient.helper import BugException
# from pythonrecordingclient.watchdog import Watchdog

logger = logging.getLogger('prc')

class ObjectStore(object):
    isRunning = 1
    isReadBlocking = False # For a workaround, see comment in stop_gracefully

    outfile: Union[None, bool, BinaryIO] = None
    watchdog = None

    print_output = False

    # Session object is usually used as a parameter but we need a global ref for stop_gracefully
    session: Optional[Session] = None
    stream_adapter: Optional[BaseAdapter] = None

def stop_gracefully(signal, frame):
    logger.info('Signal caught, exiting.')
    if ObjectStore.isRunning == 0:
        logger.warning("Recordingclient was already supposed to stop but did not. Forcing exit.")
        sys.exit()
    else:
        ObjectStore.isRunning = 0
        # isRunning has no effect if we're not done connecting yet
        if ObjectStore.session is not None and not ObjectStore.session.ready:
            ObjectStore.session.stop()
        elif ObjectStore.isReadBlocking:
            Thread(target=readBlockWorkaround, name="readBlockWorkaround", daemon=True).start()

def readBlockWorkaround() -> None:
    """
    This is a workaround for the blocking portaudio read function.
    If the audio read function in send_audio keeps blocking, isRunning
    will never be checked and the program hangs.
    So we skip most of the cleanup that would happen in send_audio and
    just signal the session to stop, which results in all remaining non-daemon threads
    being closed and therefore having the daemon-processes including the
    blocking send_audio loop stopped uncleanly too.

    A real solution would be to switch to using the non-blocking API of portaudio,
    but currently there's not enough reason to switch.
    """
    # Wait a second and check again to see if we're really blocked or just had bad luck
    import time
    time.sleep(1)
    if ObjectStore.isReadBlocking:
        logger.warning("Audio read is blocking, using workaround to stop.")
        if ObjectStore.session is not None:
            ObjectStore.session.stop()
        else:
            # Shikataganai
            logger.warning("Workaround failed. Forcing close")
            sys.exit()



def verify_chunk_size(value: Union[str, int]) -> int:
    try:
        val: int = int(value)
        assert(val > 0)
    except:
        raise argparse.ArgumentTypeError('%s is an invalid positive int value' % value)
    return val

def configure_logging(level) -> None:
    logger.setLevel(level)
    logger.propagate = False
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(level)
    formatter = logging.Formatter(fmt='[{asctime}][{levelname:^8}]: {message}', datefmt='%H:%M:%S', style='{')
    ch.setFormatter(formatter)
    logger.addHandler(ch)


@on_receive
def session_receive(session: Session, el) -> None:
    if not ObjectStore.print_output or el.find('text') is None or el.find('text').text is None:
        return
    start = el.get('start')
    stop = el.get('stop')
    startOf = el.get('startoffset')
    stopOf = el.get('stopoffset')
    creator = el.get('creator')
    fingerprint = el.get('fingerprint')
    text = urllib.parse.unquote(el.find('text').text)
    formatted = ' | '.join([creator, str(session.id), fingerprint, startOf, stopOf, start, stop]) + ': ' + text
    if ObjectStore.watchdog:
        ObjectStore.watchdog.received_text(fingerprint, formatted)
    #print(formatted)
    with ObjectStore.gui.lock:
        ObjectStore.gui.newLogs.append(formatted)

@on_session_ready
def session_ready(session: Session) -> None:
    if ObjectStore.outfile:
        try:
            ObjectStore.outfile = open('{}-{}-{}.raw'.format(datetime.now().isoformat(timespec='minutes'), session.id, session.title), mode='wb')
        except OSError as e:
            logger.warning("Could not open file {} to write audio. Reason: {}".format(e.filename, e.strerror))

    t1 = Thread(target=send_audio, args=(session, ))
    t1.daemon = True
    t1.start()

def send_audio(session: Session) -> None:
    logger.info(f'Session ({session.id}) ready, sending audio..')
    stream_adapter = ObjectStore.stream_adapter
    assert stream_adapter is not None

    try:
        while ObjectStore.isRunning:
            ObjectStore.isReadBlocking = True
            chunk = stream_adapter.read()
            ObjectStore.isReadBlocking = False

            chunk = stream_adapter.chunk_modify(chunk)

            if len(chunk) == 0:
                continue

            if ObjectStore.outfile:
                try:
                    if isinstance(ObjectStore.outfile, BinaryIO):
                        ObjectStore.outfile.write(chunk)
                    else:
                        logger.warning("Outfile is not writable, disabling. This is possibly a bug")
                        ObjectStore.outfile = False
                except OSError as e:
                    logger.warning("Could not write audio to file. Reason: {}".format(e.strerror))

            session.send_audio(chunk)

        logger.info("Exited audio loop. Cleaning up...")

        session.stop()
        logger.debug("Stopped session")
        stream_adapter.cleanup()

        if ObjectStore.outfile:
            try:
                if isinstance(ObjectStore.outfile, BinaryIO):
                    ObjectStore.outfile.close()
                    logger.debug("Closed file")
            except Exception as e:
                logger.warning("Could not close file. Error follows")
                logger.warning(e)
        logger.info("Done. Goodbye")
    except IOError as e:
        logger.error('IOError in stream.read(), may be caused by an outdated version portaudio?')
        try:
            stream_adapter.cleanup()
        except Exception as e:
            logger.error("Unhandled error:")
            logger.error(e)
        sys.exit()

def print_all_workers(server: str, port: int = 4443,
        auth: Optional[Auth] = None) -> None:
    """
    Special command, queries Mediator for Workers and prints them.
    """
    from pythonrecordingclient.mediatorLib import get_worker_list
    import json
    workers = get_worker_list(server, port, auth)
    # Pretty print
    if workers is not None:
        print(json.dumps(json.loads(workers), sort_keys=True, indent=4))

def get_title_from_api(hall_id, api: str) -> str:
    r = requests.get(args.api + '/events/?hall={}&offset_pre=30&offset_post=30'.format(args.hall))
    result = r.json()
    events = []
    for event in result['events']:
        start = datetime.strptime(event['start'], '%Y-%m-%dT%H:%M:%S')
        stop = datetime.strptime(event['stop'], '%Y-%m-%dT%H:%M:%S')
        event['cid'] = '{}_{}'.format(event['slug'], start.strftime('%y%m%d%H%M'))
        event['start'] = start.strftime('%Y-%m-%d %H:%M')
        event['stop'] = stop.strftime('%Y-%m-%d %H:%M')
        events.append(event)

    if len(events) == 0:
        print('No current events found')
        exit(0)

    for i, event in enumerate(events):
        print(str(i) + ')')
        print('slug:  ' + event['slug'])
        print('title: ' + event['title'])
        print('start: ' + event['start'])
        print('stop:  ' + event['stop'])
        print('\n')

    print('Which of the listed events should be started (enter number)?')
    num = eval(input('> '))
    while True:
        try:
            event = events[int(num)]
            break
        except (ValueError, IndexError):
            print('You Input could not be parsed. Try again:')
            num = eval(input('> '))
    return '{title}?N=L?T=en?SUB=en?S={slug}?D={start}?C={cid}'.format(**event)

def main(args: argparse.Namespace) -> None:
    configure_logging(getattr(logging, args.log.upper()))

    auth = Auth(args.user, args.passw, args.authserver) \
            if args.user is not None else None

    if args.list_workers:
        print_all_workers(args.server, args.port, auth)
        exit(0)

    input_t = args.input
    stream_adapter: Optional[BaseAdapter] = None
    if input_t == 'portaudio':
        logger.info("Using portaudio as input.")
        stream_adapter = PortaudioStreamGetAudio(callback=args.callback, chunksize=args.chunksize, audiofileid=args.audiofileid)
        input = args.audiodevice
        if args.list:
            stream_adapter.print_all_devices()
            exit(0)
        if args.audiodevice < 0:
            logger.error("The portaudio backend requires the '-a' parameter.")
            exit(1)
    elif input_t == 'ffmpeg':
        stream_adapter = FfmpegStream(pre_input=args.ffmpeg_pre, post_input=args.ffmpeg_post,
                chunksize=1024, volume=args.volume, repeat_input=(not args.once))
        input = args.ffmpeg_input
        if args.ffmpeg_input is None:
            logger.error("The ffmpeg backend requires an url/file via the '-f' parameter")
            exit(1)
    else:
        raise BugException()
    ObjectStore.stream_adapter = stream_adapter
    ObjectStore.gui = args.gui

    if args.hall:
        title = get_title_from_api(args.hall, args.api)
    else:
        title = args.title

    if args.outputfile:
        ObjectStore.outfile = True

    ObjectStore.print_output = args.printoutput

    output_fingerprint = args.outfingerprint.split(',') if args.outfingerprint else []
    session = Session(
            name=title, description=args.desc, password=args.password,
            input_fingerprint=args.fingerprint, output_fingerprint=output_fingerprint,
            input_types=['audio'], logging=args.store, interactive=True,
            zoom_url=args.zoomurl, zoom_quick_cc=args.zoomquickcc,
            auth = auth)
    ObjectStore.session = session

    if not stream_adapter.available():
        exit(1)

    stream_adapter.set_input(input)
    if args.audiochannel is not None:
        if not isinstance(stream_adapter, PortaudioStream):
            logger.error("Audio channels are currently only supported with portaudio TODO")
            exit(1)
        stream_adapter.set_audio_channel_filter(args.audiochannel)

    session.start(args.server, args.port, http_tunnel=args.proxy)
    assert session.interface.socket.t is not None, RuntimeError("Session start failed.")
    #session.interface.socket.t.join(99999)
    #sys.exit(session.interface.exit_code)

class PortaudioStreamGetAudio(PortaudioStream):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self.callback = kwargs["callback"]
        self.audiofileid = kwargs["audiofileid"]

        self.converting = False
        self.lock = Lock()

    def read(self, chunksize: Optional[int] = None) -> bytes:
        chunk = super().read(chunksize=chunksize)
        with self.lock:
            if not self.converting:
                self.callback(self.audiofileid, chunk)
        return chunk

    def convert(self):
        def convert_(id):
            try:
                with open(id+'.raw', 'rb') as f:
                    tmp = f.read()
                tmp = np.frombuffer(tmp, dtype=np.int16)
                wf.write(id+'.wav', 16000, tmp)
                os.remove(id+'.raw')
            except FileNotFoundError:
                pass
        c = Thread(target=convert_, args=(self.audiofileid,), daemon=True)
        with self.lock:
            self.converting = True
            c.start()

# If we are not running interactively
if __name__ == '__main__':

    signal.signal(signal.SIGINT, stop_gracefully)

    parser = argparse.ArgumentParser(description='RecordingClient that streams audio from different backends.\n\n'
            'To use select an input method via the "-i" flag. Currently supported are portaudio and ffmpeg\n'
            'When using a microphone or generally a hardware device, select portaudio.\nFor files (not raw)'
            'and network streams (everything that ffmpeg can detect automatically) use ffmpeg.\n\n'
            'Support for raw files (without headers) will be added later.'
            '\nFor the while being use the legacy "file" script TODO\n\n'
            'portaudio options: -L, -a, -ch\n\n'
            'ffmpeg options: -f --ffmpeg-pre --ffmpeg-post --volume --once\n'
            'ffmpeg tip: Local files will sent as fast as possible, use --ffmpeg-pre="-re" to send in realtime\n'
            'ffmpeg tip: Use --once to not loop the input file (especially if not using the realtime send tip)\n',
            formatter_class=argparse.RawDescriptionHelpFormatter
            )

    """
    General options
    """
    parser.add_argument('-i', '--input', help="Which input type should be used", choices=['portaudio', 'ffmpeg'], default='portaudio')
    parser.add_argument('-S', '--server', help='Mediator server address')
    parser.add_argument('--list-workers', help="Alternative command. Query the Mediator for workers and print them", action='store_true')
    parser.add_argument('-H', '--hall', help='lecture hall id', default='')
    parser.add_argument('-A', '--api', help='URL to event API', default='https://lecture-translator.kit.edu')
    parser.add_argument('-p', '--port', type=int, help='port', default=4443)
    parser.add_argument('-t', '--title', help='title of the session', default='TestSession')
    parser.add_argument('-d', '--desc', help='description of the session', default='')
    parser.add_argument('-w', '--password', help='password of the session. This is not related to user/pass.', default='secret')
    parser.add_argument('-fi', '--fingerprint', help='fingerprint of the input stream', default='de-DE')
    parser.add_argument('-fo', '--outfingerprint', help='fingerprint of the output stream', default='')
    parser.add_argument('-po', '--printoutput', help='print the output stream', action='store_true')
    parser.add_argument('-s', '--store', help='store session in database', action='store_true')
    parser.add_argument('-r', '--record', help='record session in database (not implemented)', action='store_true')
    parser.add_argument('-c', '--chunksize', type=verify_chunk_size, help='size of chunks, that are sent to the mediator', default=1024)
    parser.add_argument('-l', '--log', help='logging level', default='info', choices=['critical', 'error', 'warn', 'info', 'debug'])
    parser.add_argument('--zoomurl', help='Zoom video conference url for supplying subtitles', default=None, type=str)
    parser.add_argument('--zoomquickcc', help='Display Zoom subtitles in a fast, but not reliable way', action='store_true')
    parser.add_argument('--proxy', help="url to a http tunnel proxy 'https?://\w+(:\d+)?'", default=None, type=str)
    parser.add_argument('--user', help="Username for connecting with the authentication server. Needs to be used with --pass. Setting enables authentication.", default=None, type=str)
    parser.add_argument('--pass', help="Password for connecting with the authentication server. If not supplied you will be asked to enter it.", default=None, type=str, dest="passw")
    parser.add_argument('--authserver', help="Server used for logging in with --user and --pass", default="https://auth.lecture-translator.com", type=str)
    parser.add_argument('-o', '--outputfile', help='dump raw audio to file', action='store_true')

    """
    PyAudio/Portaudio, old "live" script
    """
    parser.add_argument('-L', '--list', help='Pyaudio. List audio available audio devices', action='store_true')
    parser.add_argument('-a', '--audiodevice', help='Pyaudio. Index of audio device to use', default=-1, type=int)

    parser.add_argument('-ch', '--audiochannel', help='index of audio channel to use (first channel = 1)', type=int, default=None)

    """
    Ffmpeg
    """
    parser.add_argument('-f', '--ffmpeg-input', help='Input file/address that will be given to ffmpeg', type=str)
    parser.add_argument('--ffmpeg-pre', help='ffmpeg options inserted before input parameter (-f).'
            'Don\'t forget to escape via string so this will be one single parameter.'
            'The parameter will be delimited at whitespace and does not support escaping', type=str)
    parser.add_argument('--ffmpeg-post', help='ffmpeg options inserted after input parameter (-f).'
            'Don\'t forget to escape via string so this will be one single parameter.'
            'The parameter will be delimited at whitespace and does not support escaping', type=str)
    parser.add_argument(
            '--volume', help='Adjust the volume via ffmpeg', type=float, default=1.0)
    parser.add_argument('--once', help='Run the ffmpeg input once instead of repeatedly',
            action='store_true')


    args = parser.parse_args()
    if args.zoomurl=="":
        args.zoomurl = None

    try:
        import pkg_resources
        version = pkg_resources.require("PythonRecordingClient")[0].version
        logger.info("PythonRecordingClient Version {} for Python3".format(version))
    except:
        logger.info("PythonRecordingClient Version ?")
    print()

    if not "help" in args:
        main(args)
