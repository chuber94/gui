
from PyQt5 import QtCore, QtWidgets
from gui_record import Ui_MainWindow

import datetime

from python_recording_client import PortaudioStreamGetAudio
from pythonrecordingclient.inputStreamAdapter import PortaudioStream

import threading

def write(audiofileid: str, chunk: bytes):
    with open(audiofileid+'.raw', 'ab') as f:
        f.write(chunk)

def record(adapter, chunksize, stop):
    while not stop():
        adapter.read(chunksize)

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, *args, **kwargs):
        QtWidgets.QMainWindow.__init__(self, *args, **kwargs)
        self.setupUi(self)

        self.pushButton.clicked.connect(self.startRecording)
        self.pushButton_2.clicked.connect(self.stopRecording)

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.showTime)
        self.seconds = 0

        self.load_audiodevices()

        for c in self.comboBoxes:
            c.currentIndexChanged.connect(self.update_audiodevices)

    def showTime(self):
        self.seconds += 0.1

        time = QtCore.QTime(0,0,0).addSecs(int(self.seconds))
        self.timeEdit.setTime(time)

    def startRecording(self):
        self.pushButton.setEnabled(False)
        self.pushButton_2.setEnabled(True)
        self.timer.start(100)
        self.activated = [c.isEnabled() for c in self.comboBoxes]
        for c in self.comboBoxes:
            c.setEnabled(False)

        mics = [c.currentText() for c in self.comboBoxes if c.currentIndex()>0]

        location = QtCore.QStandardPaths.standardLocations(QtCore.QStandardPaths.DocumentsLocation)[0]
        audiofileids = [location+"/LT_audios/"+str(datetime.datetime.now()).replace(":","_")+"_"+str(i) for i in range(len(mics))]

        self.adapters = [PortaudioStreamGetAudio(callback=write, chunksize=self.chunksize, audiofileid=id) for m,id in zip(mics,audiofileids)]
        for a,m in zip(self.adapters,mics):
            a.set_input([k for k,v in self.audio_devices if v==m][0])
        
        self.threads = [threading.Thread(target=record, args=(a,self.chunksize, lambda: self.stop), daemon=True) for a in self.adapters]
        self.stop = False
        for t in self.threads:
            t.start()

    def stopRecording(self):
        self.pushButton.setEnabled(True)
        self.pushButton_2.setEnabled(False)
        self.timer.stop()
        for c,e in zip(self.comboBoxes,self.activated):
            c.setEnabled(e)

        self.stop = True
        for a in self.adapters:
            a.convert()

    def load_audiodevices(self):
        self.chunksize = 1024
        stream_adapter = PortaudioStream(chunksize=self.chunksize)

        self.audio_devices = [(k,v) for k,v in stream_adapter.get_audio_devices().items()]

        self.change_text = True
        self.comboBoxes = [self.comboBox,self.comboBox_2,self.comboBox_3,self.comboBox_4,self.comboBox_5]

        self.update_audiodevices()

    def update_audiodevices(self):
        if not self.change_text:
            return
        self.change_text = False

        selected_items = []
        for i,c in enumerate(self.comboBoxes):
            ind = c.currentIndex()
            if ind <= 0:
                c.setEnabled(True)
                c.clear()
                c.addItems(["Not used"]+[v for k,v in self.audio_devices if not v in selected_items])
                for i2 in range(i+1,len(self.comboBoxes)):
                    self.comboBoxes[i2].clear()
                    self.comboBoxes[i2].setEnabled(False)
                break
            else:
                self.comboBoxes[i].clear()
                self.comboBoxes[i].addItems(["Not used"]+[v for k,v in self.audio_devices if not v in selected_items])
                c.setCurrentIndex(ind)
                selected_items.append(c.currentText())
        
        self.pushButton.setEnabled(self.comboBoxes[0].currentIndex()>0)

        self.change_text = True

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())